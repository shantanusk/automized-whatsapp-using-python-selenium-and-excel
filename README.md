# automized-whatsapp-using-python-selenium-and-excel

Automise Whatsapp Message Using Python Salenium And Excel.

This is a simple script for Web WhatsApp Bot developed in python using Selenium. 

  - Send messages to number and contacts automatically
  - Send the message in group also

### Requirements

* [Python 3+](https://www.python.org/download/releases/3.0/?) - Pyhton 3.6+ verion
* [Selenium](https://github.com/SeleniumHQ/selenium) - Selenium for web automation
* [openpyxl](https://pypi.org/project/openpyxl/) - To read xls files

### Installation For Debian Based System

Step 1: sudo apt install python3-pip 

Step 2: pip3 install selenium

Step 3: LATEST=$(wget -q -O - http://chromedriver.storage.googleapis.com/LATEST_RELEASE)

Step 4: wget http://chromedriver.storage.googleapis.com/$LATEST/chromedriver_linux64.zip

Step 5: unzip chromedriver_linux64.zip && sudo ln -s $PWD/chromedriver /usr/local/bin/chromedriver

Step 6: python3 whatsapp-with-excel-and-selenium.py

Step 7: When the browser is opened for first time web.whatsapp.com it will ask to scan a QR code

### Note

You can also add Names of the contact you want to send message in the Contacts.txt file.
The contact name should match exactly with the name saved in your contacts.

